from django.urls import re_path, path
from .views import index, jadwal_post, jadwal_table, hapus
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^jadwal_post', jadwal_post, name='jadwal_post'),
    re_path(r'^form/jadwal_table', jadwal_table, name='jadwal_table'),
	path('form/hapus', hapus)
	
]
