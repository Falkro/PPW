from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Jadwal_Form
from .models import Jadwal


def index(request):
	response = {'jadwal_form':Jadwal_Form}
	return render(request, 'form.html', response)

response={}
def jadwal_post(request):

	if request.method == 'POST':
		form = Jadwal_Form(request.POST or None)
		if form.is_valid():
			response['name'] = request.POST['name']
			response['place'] = request.POST['place']
			response['time'] = request.POST['time']
			response['category'] = request.POST['category']
			response['message'] = request.POST['message']
			jadwal = Jadwal(name=response['name'], place=response['place'],
			time=response['time'], category=response['category'],
			message=response['message'])
			jadwal.save()
			return render(request, "jadwal_masuk.html", response)
		else:
			response['form'] = form
			return render(request, 'form.html', response)
	else:
		return HttpResponseRedirect('/form/')

def jadwal_table(request):
    jadwal = Jadwal.objects.all().order_by("time")
    response['jadwal'] = jadwal
    html = 'table.html'
    return render(request, html , response)

def hapus(request):
	Jadwal.objects.all().delete()
	return render(request, 'hapus.html')

