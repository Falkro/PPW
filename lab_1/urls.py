from django.urls import re_path
from .views import index, register
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^register/', register, name='register')
]
